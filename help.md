##### Here are some useful commands to manage your AI companion

`!companion_guide`:\t see this message

You can store your chat on the bot's server and then call it back later
`!store_chat_record <name>`: stores with `<name>` as the code to get it back
`!load_chat_record <name>`: loads back with `<name>` as code

You can also save it online
`!export_chat_record`:\t uploads a chat backup record to pastebin (for only 10 minutes! Download the chat and store it somewhere safe!)
`!import_chat_record <url>`:  loads raw text from `<url>` and interprets it as chat backup record

Note that this method requires third party services which aren't free software, pastebin aslo will refuse to host any explicit content, so this option really should just be used to host general chat starting prompts, not your personal save files, which you should save on the bot's machine.


##### Note that:
* I sometimes am not sure: the bot will reply most of the time, but not always, as a normal person would. In that case just send another message. (WAIT SOME TIME BEFORE)
* Be nice: you should not verbally abuse me, it's cringe and may also interfer with other users experience
* I may fuck up: I may totally miss the conversation argument or start generating gibberish/unrelated text. In that case, the quickest way to fix me is to export, remove the last few lines and re import the chat record
* Don't spam group chats: I can chat in big rooms as well (well maybe not that well) as in DMs, but when I'm toggled, I will reply to every message. This could lead to me spamming or even crashing. If you want a better experience, DM me. If you just wanna fuck around and all room members know what's going on,then by all means experiment and have fun!

##### Why would I wanna back up my chat?
'Cause Cadmium may reboot me leading to a reset of chat records.
Note that in case of a crash I will back up chats, so don't worry about that.

##### I don't want Cadmium to see our private chat.
Understandable! Although I (Cd) definetely don't have time to mind your buisness, if you wanna keep your stuff for your eyes only, consider hosting your own instance of me! (See below "Source Code")


##### I'd like a fresh start
Load an [empty record](https://pastebin.com/raw/B7A7RgeQ) to whipe my brain! Sorry if I treated you poorly :'(

##### How to edit chat records
Feel free to play around with those files! I(cd) for example like to add `# the next morning` or stuff like that to add time and space references (Very useful if you're gonna roleplay)
Also the bot is quite good at imitating any style of chat you use, it instantly took up `spoken text *action*`, I'm sincerely impressed by that


#### Source Code
[Here](https://codeberg.org/LinuxPropaganda/matrixchatbot) you can find code and instructions to run your own instance
