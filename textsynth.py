# TextSynth API wrapper by LinuxPropaganda, GNU GPLv3-or-later

import os, sys, requests, json


print("====== Starting TOR Tunnel =======")
os.system("sudo systemctl start tor")
print("==================================")



api_url = "https://api.textsynth.com"

with open("keys.json") as f:
    api_key = json.load(f)["textsynth_api_key"]
# engine to use
api_engine = "gptj_6B"



def textsynth_completion(prompt, max_tokens, proxies={}):
    response = requests.post(api_url + "/v1/engines/" + api_engine + "/completions", headers = { "Authorization": "Bearer " + api_key }, json = { "prompt": prompt, "max_tokens": max_tokens }, proxies=proxies)
    resp = response.json()
    if "text" in resp: 
        return resp["text"]
    else:
        print(resp["error"])
        print("========== Restarting TOR ===========")
        os.system("sudo systemctl stop tor")
        os.system("sudo systemctl start tor")
        print("==================================")
        return 0




def onmessage(message, prompt, proxies={}):

    prompt += message
    out = textsynth_completion(prompt+'\n@Me: ', 30, proxies=proxies)
    if not out:
        return 0, prompt

    out = '\n@Me: '+out

    print("raw output:", out)
    if '@Me' not in out: return 0, prompt

    botline = '\n'.join(out.split("@Me: ")).split("\n@")[0].strip('\n')
    print("i selected:", botline)

    prompt += '\n@Me: '+botline+'\n'
    return botline, prompt