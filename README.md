# Tulip Chatbot for Matrix

An "AI" (neural net text synth) matrix chatbot - 'cause i got no meat buddies


## Installation

To install this thing, just run `./setup.sh`, which will
* create `keys.json`
* `pip3 install` python dependencies

You need a tor proxy (for security+fucking with api).

On debian/ubuntu based systems, run
```sudo apt install tor```
In the end you can run these commands:
```
systemctl <start/stop/status> tor
```
You may need to adjust line `22` of `main.py` according to what you end up doing.


## Running


### Preparation

#### Stuff to do in browser (Bot account and Textsynth API)
You will need to firstly create a matrix account. You can use [Element](app.element.io) from a browser to quickly do that and then to setup pfp/display name

Once you got that, you'll need to create a [textsynth](https://textsynth.com/signup.html) account (throaway emails are NOT filtered, feel free to use one). I know their model is not fully open source, but is somehow better than, say, BLOOM, especially with a short prompt. We're gonna abuse their API anyway, so you're morally safe. Then gon in the settings tab and copy your API key.

#### Configuration
Edit the file `keys.json` and fill in the details.
Note that `homeserver` should include `https://`

That's it!

### Running the script

Run the script with python3.8+
```python3 main.py```
(Run the bot in its own folder, as it'll create some files that may get in the way elsewhere. )

### Connecting to the bot

Give it some time and it'll display session id and other stuff.</br>
Now invite the bot to a room (use DMs for testing!) and verify its session.


### Start Chatting!

Use `!help` to see the [Help Page](help.md)
In short, use `!toggle_chatbot` to start chatting
