#! /bin/bash

echo '{\n"bot":{\n"homeserver":"",\n"username":"",\n"password":""\n},\n"textsynth_api_key":""\n}' > keys.json

echo -e "\e[1;33m*  Installing 'pip' dependencies"
pip3 install -r requirements.txt

echo -e "\e[1;33m*  Installing anonsurf"
git clone https://github.com/Und3rf10w/kali-anonsurf
cd kali-anonsurf
./installer.sh

echo -e "\e[1;33m* cleanin' up"
cd ..; rm -r kali-anonsurf

echo -e "\e[1;32m* Done\e[0m (unless errors showed up)"