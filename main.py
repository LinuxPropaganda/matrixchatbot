import simplematrixbotlib as botlib
import os, requests, json
import textsynth



PREFIX = '!'
chats = {}

if not os.path.exists("saves"):
    os.mkdir("saves")


with open("help.md") as f:
    COMPANION_GUIDE = f.read()


with open("keys.json") as f:
    keys = json.load(f)["bot"]

creds = botlib.Creds(keys["homeserver"], keys["username"], keys["password"])


config = botlib.Config()
config.encryption_enabled = True
config.emoji_verify = True
config.ignore_unverified_devices = False
config.store_path = './crypto_store/'

bot = botlib.Bot(creds)


tor_port = "9050"
proxies = {
        'http': 'socks5h://127.0.0.1:'+tor_port,
        'https': 'socks5h://127.0.0.1:'+tor_port
}


class Commands:

    def __init__(s):
        s.cb=0

    async def echo(s, room, message, args):

        await bot.api.send_text_message( room.room_id, " ".join(args) )


    async def help(s, room, message, args):
        await bot.api.send_markdown_message(room.room_id, COMPANION_GUIDE)


    async def toggle_chatbot(s, room, message, args):

        s.cb=not s.cb
        await bot.api.send_markdown_message(room.room_id, "# Chatbot is now"+'IN'*(not s.cb)+"ACTIVE\n"+COMPANION_GUIDE*s.cb)


    async def import_chat_record(s, room, message, args):

        if not len(args):
            await bot.api.send_text_message(room.room_id, "URL argument is required")
            return

        await bot.api.send_markdown_message(room.room_id, f"Trying to load [chat record]({args[0]})")
        await bot.api.send_text_message(room.room_id, "Oh btw if you're trying sketchy things, I'm under TOR, what you're grabbing is not my IP <3")
        txt = requests.get(args[0], proxies=proxies).text
        chats[room.room_id] = txt
        s.cb=1
        await bot.api.send_markdown_message(room.room_id, "Loaded text. Set chatbot to ACTIVE\n## Bot Initialized\n##### Here's the last 10 lines of your convo")
        await bot.api.send_text_message(room.room_id, '\n'.join(txt.split('\n')[-10:]))


    async def export_chat_record(s, room, message, args):

        link = requests.post("https://pastebin.com/api/api_post.php", {"api_dev_key":"wURY0PNk0fgjNipSnwqxiQ15V-I_3pX3", "api_paste_expire_date":"10M", 'api_paste_code': chats.get(room.room_id, "You have no active/past conversation in this chat"), 'api_option':'paste'}).text
        await bot.api.send_markdown_message(room.room_id, "Exported [chat record]("+link+")")
        await bot.api.send_markdown_message(room.room_id, "(This paste will expires in 10 minutes, grab it fast!)")


    async def store_chat_record(s, room, message, args):

        if not len(args):
            await bot.api.send_text_message(room.room_id, "NAME argument is required")
            return

        if '.' in args[0] or '~' in args[0] or '/' in args[0]:
            await bot.api.send_text_message(room.room_id, "Yeah you're such a smart h4xx0r")
            return


        if not os.path.exists("saves/"+room.room_id):
            os.mkdir("saves/"+room.room_id)

        with open("saves/"+room.room_id+'/'+args[0], 'w') as f:
            f.write(json.dumps(chats[room.room_id]))

        await bot.api.send_markdown_message(room.room_id, f"Succesfully saved as `{args[0]}`")


    async def load_chat_record(s, room, message, args):

        if not len(args):
            await bot.api.send_text_message(room.room_id, "NAME argument is required")
            return

        if '.' in args[0] or '~' in args[0] or '/' in args[0]:
            await bot.api.send_text_message(room.room_id, "Yeah you're such a smart h4xx0r")
            return

        if not os.path.exists("saves/"+room.room_id+'/'+args[0]):
            await bot.api.send_text_message(room.room_id, "No such save file: "+args[0])
            return

        with open("saves/"+room.room_id+'/'+args[0]) as f:
            chats[room.room_id] = txt = json.load(f)

        s.cb=1

        await bot.api.send_markdown_message(room.room_id, "Loaded text. Set chatbot to ACTIVE\n## Bot Initialized\n##### Here's the last 10 lines of your convo")
        await bot.api.send_text_message(room.room_id, '\n'.join(txt.split('\n')[-10:]))



commands = Commands()


@bot.listener.on_message_event
async def command(room, message):

    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot():
        if match.prefix():
            try:
                await getattr(commands, match.command())(room, message, [i for i in match.args()])
            except AttributeError:
                await bot.api.send_text_message(room.room_id, "command not found")
            except Exception as e:
                await bot.api.send_text_message(room.room_id, f"ERROR:\n{e}")


        elif commands.cb: # will automatically ignore messages starting with prefix
            out, chats[room.room_id] = textsynth.onmessage(message.sender.split(':')[0]+': '+message.body, chats.setdefault(room.room_id, "@Me: Hello everybody!\n"), proxies=proxies)
            
            if out:
                await bot.api.send_markdown_message(room.room_id, out)




print("LEZ GOOO!")

bot.run()